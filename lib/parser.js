const buffer = require('buffer');

// parsePacket decodes a packet from its data.
const parsePacket = (data) => {
    const header = {
        serial: data.readUInt16LE(0), // serial number
        tick: data.readUInt16LE(2), // device time in 100ths of a second
        type: data.readUInt8(4), // packet type
    }
    let packet = { header: header };
    switch (header.type) {
        case 0x04:
            packet.data = readConfigurationPacket(data);
            break;
        case 0x05:
            packet.data = readGPSLocationPacket(data);
            break;
        case 0x06:
            packet.data = readGPSSatelliteDataPacket(data);
            break;
        case 0x08:
            packet.data = readIMUSensorPacket(data);
            break;
        case 0x09:
            packet.data = readKalmanAndVoltagePacket(data);
            break;
        case 0x11:      // TeleMini
            packet.data = readIMUSensorPacket(data);
            break;
        case 0x12:      // TeleMega v4
            packet.data = readIMUSensorPacket(data);
            break;
        case 0x13:      // TeleMega V5
            packet.data = readIMUSensorPacket(data);
            break;
        default:
            throw 'Unsupported packet type: ' + header.type;
    }
    return packet;
}

// readConfigurationPacket reads flight computer configuration packet data.
const readConfigurationPacket = (data) => {
    return {
        type: data.readUInt8(5), // device type
        flight: data.readUint16LE(6), // flight number
        config_major: data.readUInt8(8), // config major version
        config_minor: data.readUInt8(9), // config minor version
        apogee_delay: data.readUInt16LE(10), // apogee deploy delay (seconds)
        main_deploy: data.readUInt16LE(12), // main deploy altitude (meters)
        flight_log_max: data.readUInt16LE(14), // maximum flight log size (kB)
        callsign: data.toString('utf8', 16, 24), // radio operator identifier
        version: data.toString('utf8', 24, 32), // software version identifier
    };
}

// readGPSLocationPacket reads GPS location packet data.
const readGPSLocationPacket = (data) => {
    return {
        flags: data.readUInt8(5), // gps flags
        altitude: data.readInt16LE(6), // meters
        latitude: data.readInt32LE(8), // degrees * 107
        longitude: data.readInt32LE(12), // degrees * 107
        year: data.readUInt8(16),
        month: data.readUInt8(17),
        day: data.readUInt8(18),
        hour: data.readUInt8(19),
        minute: data.readUInt8(20),
        second: data.readUInt8(21),
        pdop: data.readUInt8(22), // * 5
        hdop: data.readUInt8(23), // * 5
        vdop: data.readUInt8(24), // * 5
        mode: data.readUInt8(25), // GPS mode
        ground_speed: data.readUInt8(26), // cm/s
        climb_rate: data.readUInt8(28), // cm/s
        course: data.readUInt8(30), // / 2
    };
}

// readGPSSatelliteDataPacket reads a packet containing info about connected GPS satellites.
const readGPSSatelliteDataPacket = (data) => {
    let content = {
        channels: data.readUInt8(5), // number of satellites
        sats: [],
    };
    const start = 6;
    const end = 12 * 2 + start; // 12 * 2 bytes each
    for (let i = start; i < end; i += 2) {
        content.sats.push({
            svid: data.readUInt8(i),
            c_n_1: data.readUInt8(i+1),
        });
    }
    return content;
}

// readIMUSensorPacket reads a TeleMega v3+ sensor packet data.
// Packets are sent once per second on the ground, 10 times per second
// during ascent and once per second on descent and landing.
const readIMUSensorPacket = (data) => {
    return {
        orient: data.readUInt8(5), // angle from vertical in degrees
        accel: data.readInt16LE(6), // high G accelerometer
        pres: data.readInt32LE(8), // pressure (Pa * 10)
        temp: data.readInt16LE(12), // temperature (*C * 100)
        accel_x: data.readInt16LE(14), // X axis acceleration (across)
        accel_y: data.readInt16LE(16), // Y axis acceleration (along)
        accel_z: data.readInt16LE(18), // Z axis acceleration (through)
        gyro_x: data.readInt16LE(20), // X axis rotation (across)
        gyro_y: data.readInt16LE(22), // Y axis rotation (along)
        gyro_z: data.readInt16LE(24), // Z axis rotation (through)
        mag_x: data.readInt16LE(26), // X field strength (across)
        mag_y: data.readInt16LE(28), // Y field strength (along)
        mag_z: data.readInt16LE(30), // Z field strength (through)
    };
}

// readKalmanAndVoltagePacket reads kalman and voltage data.
// Packets are sent once per second on the ground, 5 times per second
// during ascent, and once per second on descent and landing.
const readKalmanAndVoltagePacket = (data) => {
    return {
        state: data.readUInt8(5), // flight state
        v_batt: data.readInt16LE(6), // battery voltage
        v_pyro: data.readInt16LE(8), // pyro battery voltage
        sense: [ // pyro continuity sense
            data.readInt8(10),
            data.readInt8(11),
            data.readInt8(12),
            data.readInt8(13),
            data.readInt8(14),
            data.readInt8(15),
        ],
        ground_pres: data.readInt32LE(16), // avg baro reading on ground
        ground_accel: data.readInt16LE(20), // avg accek reading on ground
        accel_plus_g: data.readInt16LE(22), // accel calibration at +1g
        accel_minus_g: data.readInt16LE(24), // accel calibration at -1g
        acceleration: data.readInt16LE(26), // m/s^2 * 16
        speed: data.readInt16LE(28), // m/s * 16
        height: data.readInt16LE(30), // m
    };
}

const readHexByte = (text, offset) => {
    return parseInt(text.substring(offset, offset+2), 16);
}

const telemetryLinePrefix = "TELEM ";
const telemetryPacketSize = 32;

// parseTelemetryLine parses a line of telemetry data from a TeleDongle.
// See: https://altusmetrum.org/AltOS/doc/telemetry.html#_teledongle_serial_packet_format
const parseTelemetryLine = (line) => {
    if (line == null || line.length < telemetryLinePrefix.length) {
        throw 'Invalid telemetry data';
    }
    var i;
    for (i = 0; i < telemetryLinePrefix.length; i++) {
        if (line[i] != telemetryLinePrefix[i]) {
            throw 'Missing prefix';
        }
    }
    const len = readHexByte(line, i);
    i += 2;
    var sum = 0;
    var packet = Buffer.alloc(telemetryPacketSize);
    for (var j = 0; j < packet.length; j++) {
        const v = readHexByte(line, i);
        packet[j] = v;
        sum += v;
        i += 2;
    }
    const rssi = readHexByte(line, i);
    sum += rssi;
    i += 2;
    const lqi = readHexByte(line, i);
    sum += lqi;
    i += 2;
    const checksum = readHexByte(line, i);
    i += 2;
    if (checksum != ((0x5a + sum) % 256)) {
        throw 'Bad checksum';
    }
    return {
        packet: packet,
        rssi: rssi,
        lqi: lqi,
        checksum: checksum,
    };
}

/*
Test parsing example telemetry

var data = parseTelemetryLine('TELEM 224f01080b05765e00701f1a1bbeb8d7b60b070605140c000600000000000000003fa988')
console.log(parsePacket(data.packet));
*/

module.exports = [parsePacket,parseTelemetryLine]
